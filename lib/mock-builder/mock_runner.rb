require 'mock-builder/container_image'
require 'mock-builder/cmd_runner'
require 'fileutils'

module MockBuilder
  class MockRunner
    include CMDRunner

    # @param src_rpm String name of the source RPM file, cannot be a path.
    # @param image ContainerImage The image used for running the container
    # @param mock_chroot String The chroot to use in mock. By default x86_64 Fedora Rawhide
    # @param result_dir String The name of where the resulting artifacts will be placed 'results' by default.
    def initialize(src_rpm, image, mock_chroot: 'fedora-rawhide-x86_64', result_dir: 'results')
      @chroot = mock_chroot
      @src_rpm = src_rpm
      @src_rpm_path = File.expand_path(src_rpm)
      @cont_image = image
      @host_result_dir = File.expand_path result_dir
      @container_result_dir = result_dir

      @cache_dir = File.expand_path '.mock_build.cache'
    end

    # Prepare the Directories. Deletes and creates the cache dir ('.mock_build.cache') and the +@result_dir+
    # Then hand over the process via exec to podman.
    def run
      FileUtils.rm_rf(@host_result_dir)
      FileUtils.mkdir_p(@host_result_dir)

      FileUtils.rm_rf(@cache_dir)
      FileUtils.mkdir_p(@cache_dir)

      FileUtils.copy_file(@src_rpm_path, File.join(@cache_dir, @src_rpm), preserve: true)

      src_rpm_container_path = '/opt/app-root/src_rpm/' + @src_rpm

      command = [
        'podman',
        'run',
        '-it',
        '--rm',
        '--cap-add=CAP_SYS_ADMIN',
        '--userns', 'keep-id',
        '-v', "#{@host_result_dir}:/opt/app-root/#{@container_result_dir}:z,rw",
        '-v', "#{@cache_dir}:/opt/app-root/src_rpm/:Z",
        @cont_image,
        @chroot, src_rpm_container_path,
        '--resultdir', @container_result_dir
      ]

      puts "Executing mock with: " + command.join(' ')

      exec(*command)
    end
  end
end
